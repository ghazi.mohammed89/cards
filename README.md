# CARDS

C'est un mini-projet qui permet de construire une main de 10 cartes de manière aléatoire à partir de cette liste de cartes en dessous :

--> Carreaux, Coeur, Pique, Trèfle (As, 5, 10, 8, 6, 7, 4, 2, 3, 9, Dames, Roi, Valet)

Une deuxième étape qui permet de trier les cartes construit.


### Installation

Pour démarrer le projet il faut lancer les commandes suivantes : 


```bash
$ composer install
$ symfony server:start
```


### Tests

Pour tester il faut laacer la commande suivante :

```bash
$ symfony php bin/phpunit
```




### Tests Nos API

1) Vérifier l'état de notre mini projet

```curl
curl --location --request GET 'http://127.0.0.1:8000/health'
```

2) Construire une main de 10 cartes de manière aléatoire.

```curl
$ curl --location --request GET 'http://127.0.0.1:8000/cards'
```

3) Construire une main de 10 cartes de manière aléatoire et triés.


```curl
curl --location --request GET 'http://127.0.0.1:8000/cards?sort=1'
```