<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Domain\Card\Card;
use App\Domain\Card\Service\CardHandService;
use PHPUnit\Framework\TestCase;

class CardHandServiceTest extends TestCase
{
    private CardHandService $cardHandService;

    protected function setUp(): void
    {
        $this->cardHandService = new CardHandService();
    }


    public function testRandomCards()
    {
        $cards = $this->cardHandService->randomCards();
        $this->assertCount(10, $cards);

        foreach ($cards as $card) {
            $this->assertInstanceOf(Card::class, $card);
        }
    }

    public function testSortCards()
    {
         // Create some Card objects to sort
        $cards = [
            new Card('Trèfle', 'As'),
            new Card('Carreaux', '4'),
            new Card('Pique', '6'),
            new Card('Coeur', 'Valet'),
            new Card('Trèfle', '2'),
            new Card('Carreaux', '3'),
            new Card('Pique', 'Dame'),
            new Card('Coeur', '5'),
            new Card('Trèfle', '10'),
            new Card('Carreaux', '7'),
        ];

        $sortedCards = $this->cardHandService->sortCards($cards);

        // Check sort Cards
        // $colors = ['Carreaux', 'Coeur', 'Pique', 'Trèfle'];
        // $values = ['As', '5', '10', '8', '6', '7', '4', '2', '3', '9', 'Dame', 'Roi', 'Valet'];

        $this->assertNotEquals($cards, $sortedCards);

        $this->assertEquals([
            new Card('Carreaux', '7'),
            new Card('Carreaux', '4'),
            new Card('Carreaux', '3'),
            new Card('Coeur', '5'),
            new Card('Coeur', 'Valet'),
            new Card('Pique', '6'),
            new Card('Pique', 'Dame'),
            new Card('Trèfle', 'As'),
            new Card('Trèfle', '10'),
            new Card('Trèfle', '2'),

        ], $sortedCards);
    }




}
