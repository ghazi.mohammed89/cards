<?php

declare(strict_types=1);

namespace App\Domain\Card\Service;

interface CardHandServiceInterface
{
    public function randomCards(): array;

    public function sortCards(array $cards): array;

    public function displayMain(array $cards): array;
}
