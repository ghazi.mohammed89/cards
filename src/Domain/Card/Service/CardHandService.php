<?php

declare(strict_types=1);

namespace App\Domain\Card\Service;

use App\Domain\Card\Card;

class CardHandService implements CardHandServiceInterface
{
    private array $colors = ['Carreaux', 'Coeur', 'Pique', 'Trèfle'];
    private array $values = ['As', '5', '10', '8', '6', '7', '4', '2', '3', '9', 'Dame', 'Roi', 'Valet'];

    public function randomCards(): array
    {
        shuffle($this->colors);
        shuffle($this->values);

        $cards = [];

        for ($i=0; $i<10; $i++)
        {
            $cart = new Card($this->colors[$i % 4], $this->values[$i]);

            $cards[] = $cart;
        }

        return $cards;
    }

    public function displayMain(array $cards): array
    {
        $hand = [];
       foreach ($cards as $card)
       {
           $hand[] = $card->getValue() . ' ' . $card->getColor();
       }

       return $hand;
    }

    public function sortCards(array $cards): array
    {
        usort($cards, [$this, 'compareCards']);

        return $cards;
    }

    private function compareCards(Card $a, Card $b): int
    {
        $aColorIndex = array_search($a->getColor(), $this->colors, true);
        $bColorIndex = array_search($b->getColor(), $this->colors, true);

        if ($aColorIndex !== $bColorIndex) {
            return $aColorIndex - $bColorIndex;
        }

        $aValueIndex = array_search($a->getValue(), $this->values, true);
        $bValueIndex = array_search($b->getValue(), $this->values, true);
        return $aValueIndex - $bValueIndex;
    }
}
