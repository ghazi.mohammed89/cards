<?php

declare(strict_types=1);

namespace App\Application\Controller\Cart\Read;

use App\Domain\Card\Service\CardHandServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GetCardsAction extends AbstractController
{
    #[Route('/cards', name: 'app.cards.list', methods: ['GET'])]
    public function __invoke(Request $request, LoggerInterface $logger, CardHandServiceInterface $cardHandService)
    {
        $cards  = $cardHandService->randomCards();

        if ($request->query->getBoolean('sort')) {
            $sortCard = $cardHandService->sortCards($cards);

            return $this->json([
                'hand' => $cardHandService->displayMain($cards),
                'hand_sort' => $cardHandService->displayMain($sortCard)
            ]);
        }


        return $this->json([
            'hand' => $cardHandService->displayMain($cards)
        ]);
    }
}
