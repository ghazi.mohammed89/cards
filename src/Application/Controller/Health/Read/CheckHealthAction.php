<?php

declare(strict_types=1);

namespace App\Application\Controller\Health\Read;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CheckHealthAction extends AbstractController
{
    #[Route('/health', name: 'app.health.check', methods: ['GET'])]
    public function __invoke(Request $request, LoggerInterface $logger)
    {
        if ($request->query->getBoolean('logs')) {
            $logger->critical('My custom logged critical.');
            $logger->error('My custom logged error.');
            $logger->warning('My custom logged warning.');
            $logger->notice('My custom logged notice.');
            $logger->debug('My custom logged debug.');
        }

        return $this->json([
            'name' => $this->getParameter('app.project_name')
        ]);
    }
}
